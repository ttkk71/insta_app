class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :username
      t.text :website
      t.text :introduce
      t.string :email
      t.integer :tel
      t.integer :sex

      t.timestamps
    end
  end
end
