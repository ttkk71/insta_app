require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:taka)
    @post = posts(:one)
  end

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: "", password: "" } }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login with valid information" do
    get login_path
    post login_path, params: { session: { email:    @user.email,
                                          password: 'password' } }
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", edit_user_path
    assert_select "a[href=?]", user_path
    assert_select "a[href=?]", user_post_path(@user,@post)
    assert_select "a[href=?]", new_user_post_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
  end
  
end
