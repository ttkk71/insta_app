require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user  = users(:taka)
    @user2 = users(:foobar)
  end

  test "showpage display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_match @user.posts.count.to_s, response.body
  end
end
