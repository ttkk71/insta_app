require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:taka)
  end

  test "layout links" do
    get root_path
    assert_template 'top_pages/home'
    get signup_path
  end
  
  test "layout links when logged in" do
    log_in_as @user
    get root_path
    assert_template "top_pages/home"
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", new_user_post_path(@user)
    assert_select "a[href=?]", user_path(@user)
  end
end
