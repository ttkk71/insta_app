require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @post    = posts(:one)
    @user    = users(:taka)
    @comment = comments(:one)
  end
  
  test "create should require logged-in user" do
    assert_no_difference 'Comment.count' do
      post user_post_comments_path(@user, @post)
    end
    assert_redirected_to login_url
  end

  test "destroy should require logged-in user" do
    assert_no_difference 'Comment.count' do
      delete user_post_comment_path(@user, @post, @comment)
    end
    assert_redirected_to login_url
  end
end
