require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @post = posts(:one)
    @user = users(:taka)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Post.count' do
      post user_posts_path(@post), params: { post: { image: "Lorem_ipsum.jpg" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Post.count' do
      delete user_post_path(@user, @post)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for wrong post" do
    log_in_as(users(:taka))
    post = posts(:one)
    assert_no_difference 'Post.count' do
      delete user_post_path(post,@user)
    end
    assert_redirected_to root_url
  end
  


end
