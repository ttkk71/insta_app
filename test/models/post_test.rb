require 'test_helper'

class PostTest < ActiveSupport::TestCase
  def setup
    @user = users(:taka)
    @post = @user.posts.build(image: "Lorem_ipsum.jpg")
  end


  test "user id should be present" do
    @post.user_id = nil
    assert_not @post.valid?
  end
  
  test "image should be present" do
    @post.image = nil
    assert_not @post.valid?
  end

  test "content should be at most 140 characters" do
    @post.comment = "a" * 141
    assert_not @post.valid?
  end
  
end
