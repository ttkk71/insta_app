require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name:"Example", username: "taka", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "    "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 21
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  
  test "tel validation should accept valid number" do
    valid_numbers = %w[08012345678 12345678910 08008080808]
    valid_numbers.each do |valid_number|
      @user.tel = valid_number
      assert @user.valid?, "#{valid_number.inspect} should be valid"
    end
  end
  
  test "tel validation should reject invalid number" do
    invalid_numbers = %w[000000000000 111 2222222 tk99999]
    invalid_numbers.each do |invalid_number|
      @user.tel = invalid_number
      assert_not @user.valid?, "#{invalid_number.inspect} should be invalid"
    end
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  
  test "should follow and unfollow a user" do
    taka     = users(:taka)
    foobar   = users(:foobar)
    assert_not taka.following?(foobar)
    taka.follow(foobar)
    assert taka.following?(foobar)
    assert foobar.followers.include?(taka)
    taka.unfollow(foobar)
    assert_not taka.following?(foobar)
  end
end
