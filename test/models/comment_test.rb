require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:taka)
    @comment = @user.comments.build(user_id: 1, post_id: 2, content: "Lorem ipsum")
  end


  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end
  
  test "post_id should be present" do
    @comment.post_id = nil
    assert_not @comment.valid?
  end
  
  test "content should be present" do
    @comment.content = "   "
    assert_not @comment.valid?
  end

  test "content should be at most 140 characters" do
    @comment.content = "a" * 51
    assert_not @comment.valid?
  end
  
end
