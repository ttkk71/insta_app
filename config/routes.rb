Rails.application.routes.draw do
  
  root                              "top_pages#home"
  get    "/signup",             to: "users#new"
  post   "/signup",             to: "users#create"
  get    "/login",              to: "sessions#new"
  post   "/login",              to: "sessions#create"
  delete "/logout",             to: "sessions#destroy"
  
  resources :users, shallow: true, only: [:index, :show, :edit, :update, :destroy] do
    member do
      get :following, :followers
    end
    resources :posts,         only: [:show, :new, :create, :destroy] do
      resources :comments,    only: [:create, :destroy]
    end
  end

  resources :password_resets, only: [:edit, :update]
  resources :relationships,   only: [:create, :destroy]
end
