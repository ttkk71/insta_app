class Post < ApplicationRecord
  belongs_to :user, optional: true
  has_many :comments, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  mount_uploader :image, ImageUploader
  validates :comment, length: { maximum: 140 }
  validates :image, presence: true
  validate  :image_size
  
  private
  
    # アップロードされた画像のサイズをバリデーションする
    def image_size
      if image.size > 5.megabytes
        errors.add(:image, "投稿できるサイズは5MB以下です。")
      end
    end
end
