class PasswordResetsController < ApplicationController
  
  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if params[:user][:password].empty?                  # パスワードが空かどうか
      flash.now[:danger] = "パスワードを入力してください。"
      render 'edit'
    elsif !!@user.authenticate(params[:user][:current_password]) && ( params[:user][:password] == params[:user][:password_confirmation] ) 
      if @user.update_attributes(user_params)       
        flash[:success] = "パスワードを更新しました！"
        redirect_to user_path @user
      else
        render "edit"
      end
    else
      flash.now[:danger] = "現在のパスワードまたは新しいパスワードが一致しません。"
      render 'edit'                         
    end
  end
  
  private
  
    def user_params
      params.require(:user).permit(:password)
    end
end
