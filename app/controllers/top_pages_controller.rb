class TopPagesController < ApplicationController
  def home
    if logged_in?
      @user =  current_user  
      @comment = Comment.new
      @post  = current_user.posts.build
      @feed_items = current_user.feed.paginate(page: params[:page], per_page: 5)
    else
      @user = User.new
    end
  end
end
