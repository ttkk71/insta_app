class PostsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def index
    @post = Post.all
  end
  
  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
    @comments = @post.comments
  end
  
  def new
    @post = Post.new
  end

  def create
    @user = params[:user_id]
    @post = current_user.posts.build(post_params)
    if @post.save
      flash.now[:success] = "投稿しました！"
      redirect_to  post_url(@post)
    else
      flash.now[:danger] = "画像が選択されていません。"
      render "new"
    end
  end

  def destroy
    @post = Post.find_by(id: params[:id])
    @post.destroy
    flash[:success] = "投稿を削除しました！"
    redirect_to user_path(current_user)
  end
  
  private
  
    def post_params
      params.require(:post).permit(:comment, :image)
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end
