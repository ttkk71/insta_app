class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy,
                                        :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  
  def index
  end
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "アカウントを作成しました!"
      redirect_to root_url
    else
      flash.now[:danger] = "無効な入力です。"
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
    @post = @user.posts.paginate(page: params[:page], per_page: 12)
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(edit_params)
      flash[:success] = "プロフィールが保存されました。"
      redirect_to edit_user_path
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      flash[:success] = "ユーザーを削除しました"
      redirect_to root_url
    else
      flash[:danger] = "ユーザーが見つかりません"
      redirect_to root_url
    end
  end
  
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follower'
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :username, :email, :password)
    end
    
    def edit_params
      params.require(:user).permit(:image, :name, :username, :website, :introduce, :email, :sex)
    end
  
end
