class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  
  def create
    @post = Post.find(params[:post_id]) 
    @comment = @post.comments.build(comment_params)
    if @comment.save
      flash[:success] = "コメントを投稿しました！"
      redirect_back(fallback_location: post_path(@post))
    else
      flash.now[:danger] = "コメントを入力してください！"
      render "posts/show"
    end
  end
  
  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      flash[:success] = "コメントを削除しました！"
      redirect_back(fallback_location: post_path(@comment.post))
    else
      render "posts/show"
    end
  end
  
  private
  
    def comment_params
      params.require(:comment).permit(:content, :user_id, :post_id)
    end
end
